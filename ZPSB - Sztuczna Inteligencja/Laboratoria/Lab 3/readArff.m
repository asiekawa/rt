% Funkcja wczytuje dane w formaci programu Weka, (c) mkorzen@wi.ps.pl 2007
% [A,PA, varnames]=readArff(fname)
% 
% A - macierz kom�rkowa zawieraj�ca dane
% PA - maciery unikalnzch wartosci posycyegolnzch atrzbutow
% varnames + nazwy zmiennych

function [A,PA, varnames]=readArff(fname)
disp(fname)

kk=0;
varnames = {};
varcount=0;
table ='';
A={};
PA={};
wiersze={};
now=0;

try
    [fid, message]=fopen(fname);
catch
    warning('b��d otwarcia pliku');
    return
end
disp('atrybuty:');
tic
while 1
    tline = fgetl(fid);
    if ~ischar(tline) 
        break 
    end    
    if isempty(tline) 
        continue 
    end    
        switch tline(1)
            case '%'
                disp(tline);
                continue
            case '@'
                %disp(tline)
                [command, R]=strtok(tline);
                if strcmpi(command,'@relation') 
                   table = strtok(R);
                   continue;
                end
                if  strcmpi(command,'@data')
                    now=1;
                    continue;
                end   
               
                if strcmpi(command,'@attribute') 
                    varcount = varcount +1;
                    [name, R]= strtok(R) ;
                    
                    [klamry] = cutstr(R, '{', '}');
                    %R=strrep(R,' ','');
                    %[klamry, R] = strtok(R)
                    varnames{varcount}=strrep(name,'''','');                  
                    %if ~strcmpi(strtrim(klamry),'real') && ...
                    %   ~strncmpi(strtrim(klamry),'integer',7) && ...
                    %   ~strncmpi(strtrim(klamry),'numeric',7)
                    if ~isempty(klamry) 
                    
                        %tmp = regexp(klamry, '\{([^\}]*)\}','tokens')
                        vals={};
                        %tmp= tmp{1};
                        
                        %tmp=tmp(2:end-1)
                        tmp=klamry;
                        tmp=strrep(tmp,'''','');
                        tmp=strrep(tmp,' ','');
                        R=tmp;
                        while true                            
                            [m, R] = strtok(R, ',') ;                           
                            if isempty(m)
                                break;
                            end
                            vals{end+1}=m;
                        end
                        PA{varcount}=vals;
                        disp(['dicrete ' num2str(varcount) ' ' tmp])  
                        varvals{varcount}=strrep(klamry,' ' ,'');
                    
                    else
                        disp(['real ' num2str(varcount)])
                        PA{varcount}={};
                    end
                   
                
                    
                end 
            otherwise
                if now
                    %disp(tline);
                    wiersze{end+1}=tline;
                end
                
        end
        kk=kk+1;
        %disp(tline);
        if  tline(1)=='%'
        end
end
toc
nn = length(wiersze);
A=cell(length(wiersze), varcount);
for ii=1:nn
    R= wiersze{ii};
    R=R(1:end);
    R=strrep(R,'''',''); 
    R=strrep(R,' ','');
    for jj=1:varcount
        [m, R]=strtok(R,',');
        if isempty(PA{jj})
            A{ii,jj}=str2double(m);
        else
            A{ii,jj}=m;
        end
    end
end
fclose(fid);
toc
