clear all
clc

[data, PA, varnames] = readArff('zoo.arff');%vote

X = data(1:end,2:end-1);
X(:,13) = cellstr(num2str(cell2mat(X(:,13))));
d = data(:, end);

bayes = Bayes();
bayes = bayes.fit(X, d);