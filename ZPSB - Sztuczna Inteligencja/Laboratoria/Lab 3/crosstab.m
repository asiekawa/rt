function [ M ] = crosstab( x, y )
    xi = unique(x);
    yi = unique(y);
    M = zeros(numel(xi), numel(yi));
    X = [x y];
    for i = 1:size(X, 1)
        M(X(i,1), X(i,2)) = 1 + M(X(i,1), X(i,2));
    end
end

