function out = cutstr(str, left, right)
if nargin==2
    le=findstr(str,left)  
    if length(le)>=2
        'a'
        out=str(le(1)+1 : le(2)-1)
    else
        out = '';
    end
elseif nargin==3
    le=findstr(str,left);
    ri=findstr(str,right);
    if ~isempty(le) && ~isempty(ri)
        out=str(le(1)+size(left) : ri(1)-1);
    else
        out = '';
    end
end