﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Obstacle
{
    Walkable,
    Wall,
    Start,
    End,
};

public enum Highlight
{
    OpenList,
    ClosedList,
    NotVisited,
    Path
}

public class GridViewer : MonoBehaviour
{

    AstarPathfinder Pathfinder;

    Obstacle CurrentDrawMode = Obstacle.Wall;

    public Sprite Tile;
    public int GridSize = 10;
    float gridScale = 600.0f;
    public Obstacle[,] GridData { get; private set; }
    Highlight[,] HighlightData;
    private Color[,] GridColors;
    GUISkin skin;

    public Vector2Int StartPoint { get; private set; }
    public Vector2Int EndPoint { get; private set; }

    Dictionary<Obstacle, Color> ColorCodes = new Dictionary<Obstacle, Color>();
    Dictionary<Highlight, Color> HighlightColorCodes = new Dictionary<Highlight, Color>();

    // Start is called before the first frame update
    void Start()
    {
        Pathfinder = new AstarPathfinder(this);

        ColorCodes.Add(Obstacle.Walkable, Color.white);
        ColorCodes.Add(Obstacle.Start, Color.green);
        ColorCodes.Add(Obstacle.End, Color.red);
        ColorCodes.Add(Obstacle.Wall, Color.yellow);

        HighlightColorCodes.Add(Highlight.OpenList, Color.cyan);
        HighlightColorCodes.Add(Highlight.ClosedList, Color.magenta);
        HighlightColorCodes.Add(Highlight.NotVisited, Color.white);
        HighlightColorCodes.Add(Highlight.Path, Color.blue);

        StartPoint = new Vector2Int(0, 0);
        EndPoint = new Vector2Int(GridSize-1, GridSize-1);
        GridData = new Obstacle[GridSize, GridSize];
        HighlightData = new Highlight[GridSize, GridSize];
        GridColors = new Color[GridSize, GridSize];
        for (int i = 0; i < GridSize; i++)
        {
            for (int j = 0; j < GridSize; j++)
            {
                GridColors[i, j] = Color.white;
                GridData[i, j] = Obstacle.Walkable;
            }
        }
        ClearHighlight();
        GridColors[0, 0] = Color.green;
        GridColors[GridSize - 1, GridSize - 1] = Color.red;

        GridData[0, 0] = Obstacle.Start;
        GridData[GridSize - 1, GridSize - 1] = Obstacle.End;
    }

    int buttonId = -1;

    // Update is called once per frame
    void Update()
    {
        if(Pathfinder.IsRunning)
        {
            Pathfinder.Step();
        }
        if (Input.GetMouseButton(0))
        {
            buttonId = 0;
        }
        else if (Input.GetMouseButton(1))
        {
            buttonId = 1;
        }
        else if (Input.GetMouseButton(2))
        {
            buttonId = 2;
        }
        else
        {
            buttonId = -1;
        }
    }

    void OnGUI()
    {
        float currentGridSize = gridScale/GridSize;

        int spacing = 10;

        if (GUI.Button(new Rect(10, 10, 70, 30), "RESET"))
        {
            if(!Pathfinder.IsRunning)
            {
                ClearHighlight();
                Pathfinder = new AstarPathfinder(this);
            }
        }

        if (GUI.Button(new Rect(10, spacing+=35, 70, 30), "FIND PATH"))
        {
            if (!Pathfinder.IsRunning)
            {
                ClearHighlight();
                Pathfinder.Initialize();
            }
        }

        spacing += 35;
        if (GUI.Button(new Rect(10, spacing += 35, 70, 30), "Place Start"))
            CurrentDrawMode = Obstacle.Start;
        if (GUI.Button(new Rect(10, spacing += 35, 70, 30), "Place End"))
            CurrentDrawMode = Obstacle.End;
        if (GUI.Button(new Rect(10, spacing += 35, 70, 30), "Draw wall"))
            CurrentDrawMode = Obstacle.Wall;

        var e = Event.current;
        if (e.type == EventType.MouseUp && e.button == 1)
            Debug.Log("Right mouse button lifted");

        Vector2 bbMax = new Vector2(currentGridSize * (GridSize - 1 - (float)GridSize / 2.0f) + Screen.width / 2, currentGridSize * (GridSize - 1 - (float)GridSize / 2.0f) + Screen.height / 2);
        Vector2 bbMin = new Vector2(currentGridSize * (-(float)GridSize / 2.0f) + Screen.width / 2, currentGridSize * (-(float)GridSize / 2.0f) + Screen.height / 2);

        Vector2 normalized = (e.mousePosition - bbMin) / (bbMax - bbMin);
        Vector2Int selected = new Vector2Int((int)((GridSize-1) * normalized.x), (int)((GridSize - 1) * normalized.y));

        if(selected.x >= 0 && selected.x < GridSize && selected.y >= 0 && selected.y < GridSize)
        {
            Obstacle current = GridData[selected.x, selected.y];
            if ((CurrentDrawMode == Obstacle.Start || CurrentDrawMode == Obstacle.End) && buttonId == 0)
            {
                if (current == Obstacle.Walkable)
                {
                    GridData[selected.x, selected.y] = CurrentDrawMode;
                    if (CurrentDrawMode == Obstacle.Start)
                    {
                        GridData[StartPoint.x, StartPoint.y] = Obstacle.Walkable;
                        StartPoint = selected;
                    }
                    if (CurrentDrawMode == Obstacle.End)
                    {
                        GridData[EndPoint.x, EndPoint.y] = Obstacle.Walkable;
                        EndPoint = selected;
                    }
                }
            }
            else if (CurrentDrawMode == Obstacle.Wall)
            {
                if (current == Obstacle.Walkable && buttonId == 0)
                {
                    GridData[selected.x, selected.y] = Obstacle.Wall;
                }
                else if (current == Obstacle.Wall && buttonId == 1)
                {
                    GridData[selected.x, selected.y] = Obstacle.Walkable;
                }
            }
        }

        for (int i = 0; i < GridSize; i++)
        {
            for (int j = 0; j < GridSize; j++)
            {
                
                GUI.contentColor = ColorCodes[GridData[i, j]];
                GUI.contentColor *= HighlightColorCodes[HighlightData[i, j]];
                if (!Pathfinder.IsRunning && i == selected.x && j == selected.y)
                {
                    GUI.contentColor *= new Color(0.75f, 0.75f, 0.75f, 1.0f);
                }
                GUI.Button(new Rect(currentGridSize * (i - (float)GridSize / 2.0f) + Screen.width / 2, currentGridSize * (j - (float)GridSize / 2.0f) + Screen.height / 2, currentGridSize, currentGridSize), Tile.texture, GUIStyle.none);
            }
        }
    }
    public void HighlightTile(Vector2Int Position, Highlight HighlightType)
    {
        HighlightData[Position.x, Position.y] = HighlightType;
    }


    public void SetHighlightColor(Highlight HighlightType, Color Color)
    {
        HighlightColorCodes[HighlightType] = Color;
    }

    public void SetObstacleColor(Obstacle ObstacleType, Color Color)
    {
        ColorCodes[ObstacleType] = Color;
    }

    public void ClearHighlight()
    {
        for (int i = 0; i < GridSize; i++)
        {
            for (int j = 0; j < GridSize; j++)
            {
                HighlightData[i, j] = Highlight.NotVisited;
            }
        }
    }

}
