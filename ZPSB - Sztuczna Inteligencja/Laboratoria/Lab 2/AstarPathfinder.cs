﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;


class AstarPathfinder
{
    public bool IsRunning { get; set; } = false;

    private GridViewer viewer;
    
    public AstarPathfinder(GridViewer Viewer)
    {
        viewer = Viewer;
    }

    public void Initialize()
    {

    }

    public void Step()
    {
    }
}

