﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns
{
    abstract class Visitor
    {
        public abstract void VisitorConcreteElementA(ConcreteElementA elementA);
        public abstract void VisitorConcreteElementB(ConcreteElementB elementB);
    }
    class ConcreteVisitor1 : Visitor
    {
        public override void VisitorConcreteElementA(ConcreteElementA elementA)
        {
            Console.WriteLine("klasa " + elementA.GetType().Name + " zostala odwiedzona przez klase " + this.GetType().Name);
        }
        public override void VisitorConcreteElementB(ConcreteElementB elementB)
        {
            Console.WriteLine("klasa " + elementB.GetType().Name + " zostala odwiedzona przez klase " + this.GetType().Name);
        }
    }
    class ConcreteVisitor2 : Visitor
    {
        public override void VisitorConcreteElementA(ConcreteElementA elementA)
        {
            Console.WriteLine("klasa " + elementA.GetType().Name + " zostala odwiedzona przez klase " + this.GetType().Name);
        }
        public override void VisitorConcreteElementB(ConcreteElementB elementB)
        {
            Console.WriteLine("klasa " + elementB.GetType().Name + " zostala odwiedzona przez klase " + this.GetType().Name);
        }
    }
    abstract class Element
    {
        public abstract void Accept(Visitor visitor);
    }
    class ConcreteElementA : Element
    {
        public override void Accept(Visitor visitor)
        {
            visitor.VisitorConcreteElementA(this);
        }
        public void OperationA()
        {
            Console.WriteLine("Operacja A");
        }
    }
    class ConcreteElementB : Element
    {
        public override void Accept(Visitor visitor)
        {
            visitor.VisitorConcreteElementB(this);
        }
        public void OperationB()
        {
            Console.WriteLine("Operacja B");
        }
    }
    class ObjectStructure
    {
        private List<Element> elements = new List<Element>();
        public void Attach(Element element)
        {
            elements.Add(element);
        }
        public void Detach(Element element)
        {
            elements.Remove(element);
        }
        public void Accept(Visitor visitor)
        {
            foreach (var a in elements)
            {
                a.Accept(visitor);
            }
        }
    }
}
