﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns
{
    abstract class Subjectt
    {
        private List<Observer> observers = new List<Observer>();
        public void Attach(Observer observer)
        {
            observers.Add(observer);
        }
        public void Dettach(Observer observer)
        {
            observers.Remove(observer);
        }
        public void Notify()
        {
            foreach (var o in observers)
            {
                o.Update();
            }
        }
    }
    class ConcreteSubject : Subjectt
    {
        private string subjectState;
        public string SubjectState
        {
            get
            {
                return subjectState;
            }
            set
            {
                subjectState = value;
            }
        }
    }
    abstract class Observer
    {
        public abstract void Update();
    }
    class ConcreteObserver : Observer
    {
        private string name;
        private string observerState;
        private ConcreteSubject subject;
        public ConcreteObserver(ConcreteSubject cSubject, string Name)
        {
            name = Name;
            subject = cSubject;
        }
        public override void Update()
        {
            observerState = subject.SubjectState;
            Console.WriteLine("Nowy stan obserwatora " + name + " to " + observerState);
        }
        public ConcreteSubject Subject
        {
            get
            {
                return subject;
            }
            set
            {
                subject = value;
            }
        }
    }
}
