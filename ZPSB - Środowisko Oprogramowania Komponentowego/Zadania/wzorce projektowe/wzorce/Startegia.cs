﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns
{
    abstract class Strategy
    {
        public abstract void AlgorithmInterface();
    }
    class ConcreteStrategyA : Strategy
    {
        public override void AlgorithmInterface()
        {
            Console.WriteLine("Strategia A");
        }
    }
    class ConcreteStrategyB : Strategy
    {
        public override void AlgorithmInterface()
        {
            Console.WriteLine("Strategia B");
        }
    }
    class ConcreteStrategyC : Strategy
    {
        public override void AlgorithmInterface()
        {
            Console.WriteLine("Strategia C");
        }
    }
    class Contex
    {
        private Strategy strategy;
        public Contex(Strategy Strategy)
        {
            strategy = Strategy;
        }
        public void ContexInteface()
        {
            strategy.AlgorithmInterface();
        }
    }
}
