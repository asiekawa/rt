﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns.Structural.Proxy
{
    abstract class Subject
    {
        public abstract void Request();
    }
    class RealSubject : Subject
    {
        public override void Request()
        {
            Console.WriteLine("real subject request");
        }
    }
    class Proxy : Subject
    {
        private RealSubject subj;
        public override void Request()
        {
            if (subj == null)
            {
                subj = new RealSubject();
            }
            subj.Request();
        }
    }
}
