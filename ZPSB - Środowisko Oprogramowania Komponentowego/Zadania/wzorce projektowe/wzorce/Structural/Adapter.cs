﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns
{    /*
        Problem:
            Nasz system ciagle uzywal parsera plikow XML, ale teraz pojawila sie potrzeba obslugi plikow JSON.
            By uniknac pisania duzej ilosci instrukcji warunkowych lub innych rozwiazan mozemy podstawic adapter z metoda
            o identycznej nazwie ktora implementuje obsluge nowego formatu plikow
    */
    abstract class Target
    {
        public abstract void Request();
    }

     class Adaptee
     {
         public void SpecificRequest()
         {
             Console.WriteLine("Specific Request");
         }

     }
     class Adapter : Target
     {
        private Adaptee adaptee;

        public Adapter(Adaptee Adaptee)
        {
            this.adaptee = Adaptee;
        }

         public override void Request()
         {
             adaptee.SpecificRequest();
         }
     }
}
