﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns.Structural.Decorator
{
    /*
    Problem:
        Pojawia sie potrzeba rozszerzenia jakiegos obiketu o nowe zachowania.
    */

    abstract class Component
    {
        public abstract void Operation();
    }

    class ConcreteComponent : Component
    {
        public override void Operation()
        {
            Console.WriteLine("operacja konretna.");
        }
    }

    abstract class Decorator : Component
    {
        protected Component component;

        public void setComponent(Component Component)
        {
            component = Component;
        }

        public override void Operation()
        {
            if (component != null)
            {
                component.Operation();
            }
        }
    }

    class ConcreteDecoratorA : Decorator
    {
        public override void Operation()
        {
            base.Operation();
            Console.WriteLine("Operacja ConcreteDecoratorA");
        }
    }
    class ConcreteDecoratorB : Decorator
    {
        public override void Operation()
        {
            base.Operation();
            addedBehavior();
            Console.WriteLine("Operacja ConcreteDecoratorB");
        }

        void addedBehavior()
        {
            Console.WriteLine("dodatkowe zachowanie dekoratora B");
        }
    }

}

/*
    Przykladowo naszym Componentem jest choinka ktora mozna udekorowac.
    Naszym ConcreteComponent jest 'zaimplementowana' choinka.
    W tej sytuacji Decorator to Dekorator Choinki.
    Nastepnie elementy ozdoby choinki to konkretne dekoratory, np. babki, swiatekla, czubek.

*/
