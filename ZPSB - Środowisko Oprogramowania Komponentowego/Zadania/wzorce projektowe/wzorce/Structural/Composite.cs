﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns.Structural.Composite
{
    /*
    Problem:
        Kiedy nasza aplikacja ma strukture drzewa. Przykladem takiej struktury jest Solution w Visual Studio :)
        Solution posiada projekty.
        Projekty posiadaja pliki kodu, foldery, referencje, własciwiosci.
        Foldery posiadaja jakies pliki.
        Referencje posiadaja assembly itd.
    */
    abstract class Component
    {
        protected string text;

        public Component(string Text)
        {
            text = Text;
        }

        public abstract void Add(Component component);
        public abstract void Remove(Component component);
        public abstract void GetChild(int ind);
        public abstract void Operation();
    }

    class Composite : Component
    {
        private List<Component> child = new List<Component>();

        public Composite(string text)
            : base(text)
        {
        }

        public override void Add(Component component)
        {
            child.Add(component);
        }

        public override void Remove(Component component)
        {
            child.Remove(component);
        }

        public override void GetChild(int ind)
        {
            Operation();
            foreach (var component in child)
            {
                component.GetChild(ind);
            }
        }

        public override void Operation()
        {
            Console.WriteLine(this.GetType().ToString() + " " + text);
        }
    }
    class Leaf : Component
    {
        public Leaf(String text)
            : base(text)
        {
        }

        public override void Operation()
        {
            Console.WriteLine(this.GetType().ToString() + " " + text);
        }

        public override void Add(Component component)
        {
            Console.WriteLine("Nie moge dodac");
        }

        public override void Remove(Component component)
        {
            Console.WriteLine("Nie moge usunac");
        }

        public override void GetChild(int ind)
        {
            Operation();
        }
    }
}
