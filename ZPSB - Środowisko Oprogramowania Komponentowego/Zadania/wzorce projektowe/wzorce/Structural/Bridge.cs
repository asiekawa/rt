﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns.Structural.Bridge
{
    /*
    Problem:
        Zalozmy ze posiadamy klase ksztalt, po ktorej dziedzicza dwie klasy - kwadrat i kolo. Do tych klas dodajemy teraz dodakowo kolor - czerwony i niebieski.
        Tworzymy klase CzerwonyKwadrat, CzerwoneKolo, NiebieskiKwadrat, NiebieskieKolo. Teraz chcemy dodac trojakt. A zaraz potem kolor zielony. 
        Chyba juz widac gdzie lezy problem, prawda? :)

        Most ma na celu rozdzielenie wymiarowosci klas i je polaczyc (stad nazwa - most). W powyzszym przykadzie wymiarowosci te to ksztalt i kolor.
    */

    abstract class Implementor
    {
        public abstract void Operation();
    }

    class ConcreteImplementorA : Implementor
    {
        public override void Operation()
        {
            Console.WriteLine("Operacja A");
        }
    }

    class ConcreteImplementorB : Implementor
    {
        public override void Operation()
        {
            Console.WriteLine("Operacja B");
        }
    }

    class Abstraction
    {
        protected Implementor implementor;

        public Abstraction(Implementor Implementor)
        {
            implementor = Implementor;
        }

        public virtual void Operation()
        {
            implementor.Operation();
        }
    }

    class RefinedAbstraction : Abstraction
    {
        public RefinedAbstraction(Implementor Implementor) : base(Implementor)
        {
        }
    }
}


/*
    Przykladowo zalozmy, ze chcemy rozdzielic sposob poruszania sie od zwierzat.
    Sposob poruszania sie to nasza implementacja.
    Chodzienie i latanie to nasza konkretna implementacja.
    Zwierzeta to nasze abstrakcje.
    Psy i papugi to nasze konkretne abstrakcje.
    Tworzac psa przypisujemy mu implementacje chodzenia.
    Tworzac papuge przypisujemy mu implementacje latania.
*/
