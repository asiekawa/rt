﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns.Creational.Prototype
{

    /*
        Problem:
            Chcemy stworzyc kopie obiektu. W tym cely mozna stworzyc instancje i wszystkie pola przekopiowac.
            Pojawia sie tu problem, poniewaz nie jestesmy w stanie skopiowac np. pol prywatnych.
    */

    class SomeClass
    {
        public string SomeField { get; set; }
    }

    abstract class Prototype
    {
        protected String text;
        protected SomeClass someClass;
        public Prototype(String Text, SomeClass SomeClass)
        {
            text = Text;
            someClass = SomeClass;
        }

        public abstract Prototype Clone();
        public abstract void Write();
    }

    class ConcretePrototype : Prototype
    {
        public ConcretePrototype(string Text, SomeClass SomeClass)
            : base(Text, SomeClass)
        {
        }

        public override Prototype Clone()
        {
            ConcretePrototype p = (ConcretePrototype)this.MemberwiseClone();
            p.text = String.Copy(this.text);
            p.someClass = new SomeClass();
            p.someClass.SomeField = String.Copy(this.someClass.SomeField);

            return p;
        }

        public override void Write()
        {
            Console.WriteLine(text);
        }
    }
}
