﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns.Creational.FactoryMethod
{
    /*
        Problem:
            Stworzylismy pewna aplikacje moniturojaca aktywnosc psow, ktora odniosla sukces. Pojawila sie potrzeba dodania obslugi kotow.
            Aplikacja zaczela dalej rosnac na popularnosci i teraz chcemy dac mozliwosc korzystania z niej wlascicielom papug.

            Wraz z rozwojem aplikacji pojawia sie potrzeba dodawania nowych elementow o cechach wspolnych (w powyzszym przykladzie sa to zwierzeta).
            dodajac kolejne nowe klasy powielamy sporo wspolnego kodu. W momencie gdy wystapi potrzeba zmiany w tym stalym kodzie bedziemy musieli ja wprowadzic
            w kazdej z klas. Dodatkowo mozemy nieopatrznie pominac jakas klase, co spowoduje wystapienie buga.

            Program ktory uzywa metody wytworczej (Client) nie widzi roznicy miedzy pomiedzy produktami w aplikacji, traktuje je jako abstrakcje "zwierzeta".
            Client wie, ze wszystkie zwierzece obiekty maja metode do monitorowania zwierzat, ale na czym te monitorowanie polega nie jest potrzebne clientowi.

    */

    interface Product
    {
        string Operation();
    }

    class ConcreteProductA : Product
    {
        public string Operation()
        {
            return "ConcreteProductA Operation";
        }
    }

    class ConcreteProductB : Product
    {
        public string Operation()
        {
            return  "ConcreteProductB Operation";
        }
    }

    abstract class Creator
    {
        public abstract Product FactoryMethod();

        public string SomeOperation()
        {
            var product = FactoryMethod();
            var result = "The same creator code worked with " + product.Operation();

            return result;
        }

    }

    class KonretnyCreator1 : Creator
    {
        public override Product FactoryMethod()
        {
            return new ConcreteProductA();
        }
    }

    class KonretnyCreator2 : Creator
    {
        public override Product FactoryMethod()
        {
            return new ConcreteProductB();
        }
    }
}
/*
 * Przyklad - PIT
 * PIT jest naszym produktem, sam PIT jest czyms abstrakcyjnym dla nas, nie okreslonym dokladnie
 * PIT ma rozne rodzaje np. PIT-37, PIT-36, PIT-11, PIT-19A itd, to jest już nasz konretny produkt
 * Dokument podatkowy jest naszym Creatorem - to jest cos abstrakcyjnego gdyz mozemy miec wiele rodzajow dokumentow
 * Dokument PIT-36 jest juz przykladem konkretnego rodzaju/klasy dokumentu podatkowego
 * PITy maja pewne elementy wspolne, np. pole okreslające rok podatkowy, miejsce i cel skaldania sprawozdania itd.
 * Kreator Dokumentu PIT-36 to jest juz nasz konkretny kreator - Creator zajmuje sie tworzeniem czesci wspolnych dla wszystkich PITow, 
 * Product tworzy elementy unikalne dla konretnego kreatora
*/
