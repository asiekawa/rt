﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns.Creational.AbstractFactory
{
    /*
        Problem:
            Wyobrazmy sobie sytuacje gdzie mamy kilka roznych rodzajow produktow dostepnych w roznych wariantach.
            Na przyklad obierzmy farby - posidamy sklep gdzie posiadamy rozne rodzaje farb farby akrylowe, emaliowe i lakierowe.
            Wszytskie te farby sa dostepne w wariancie matowym, ale nagle pojawia sie potrzeba dodania farb blyszczacych i metalicznych.

            Potrzebujemy systemu, ktory bedzie tworzyl rozne rodzaje farb w tym samym wariancie.

            Mozna rzec, ze fabryka abstrakcyjna ma na celu tworzenie roznych obiektow pochodzacych z tej samej rodziny.

    */
    abstract class ProductA
    {
        public abstract void Write();
    }
    class ProductA1 : ProductA
    {
        public override void Write()
        {
            Console.Write("produktA1");
        }
    }
    class ProductA2 : ProductA
    {
        public override void Write()
        {
            Console.Write("produktA2");
        }
    }
    abstract class ProductB
    {
        public abstract void Write();
    }
    class ProductB1 : ProductB
    {
        public override void Write()
        {
            Console.Write("produktB1");
        }
    }
    class ProductB2 : ProductB
    {
        public override void Write()
        {
            Console.Write("produktB2");
        }
    }
    abstract class AbstractFactory
    {
        public abstract ProductA GetProductA();
        public abstract ProductB GetProductB();
    }
    class ConreteFactory1 : AbstractFactory
    {
        public override ProductA  GetProductA()
        {
            return new ProductA1();
        }
        public override ProductB  GetProductB()
        {
            return new ProductB1();
        }
    }
    class ConcreteFactory2 : AbstractFactory
    {
        public override ProductA GetProductA()
        {
            return new ProductA2();
        }
        public override ProductB GetProductB()
        {
            return new ProductB2();
        }
    }
    class Client
    {
        private ProductA productA;
        private ProductB productB;
        public Client(AbstractFactory Factory)
        {
            productA = Factory.GetProductA();
            productB = Factory.GetProductB();
        }
        public void Display()
        {
            productA.Write();
            Console.WriteLine(" ");
            productB.Write();
            Console.WriteLine();
        }
    }
}