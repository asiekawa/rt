﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns.Creational.Singleton
{

    /*
        Problem:
            Potrzebujemy klasy która powinna mieć tylko jedną instancję. Instancja tej klasy powinna mieć dostęp globalny.
    */
    class Singleton
    {
        private static Singleton instance = null;

        private Singleton()
        { }

        public static Singleton Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Singleton();
                }
                return instance;
            }
        }

        public void Text()
        {
            Console.WriteLine("text singleton");
        }
    }
}

/*
    Zalety
        - Pewność, że klasa ma tylko jedną instancję
        - Globalny dostęp do instancji klasy
        - Inicjalizowany tylko raz na rządanie (leniwie)
    Wady:
        - Zakrywa ‚bad desing’ – singleton może wiedzieć za dużo o programie
        - Pojawiają się problemy przy aplikacjach wielowątkowych
        - Może być uciążliwy do testowania – ze względu na prywatny konstruktor nie da się go mockować
*/
