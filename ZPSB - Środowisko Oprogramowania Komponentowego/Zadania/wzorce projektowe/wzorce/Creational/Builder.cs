﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns.Creational.Builder
{
    /*
        Problem:
            Wyobrazmy sobie sutuacje, gdzie mamy skomplikowany obiekt ktory budujemy krok po kroku.

            Przykladem takiego obiektu moze byc budynek, ktory posiada wspolne elementy a moze byc zbudowany na rozne sposoby.

    */
    class Product
    {
        List<String> parts = new List<string>();

        public Product()
        {
        }

        public void Add(string Part)
        {
            parts.Add(Part);
        }

        public void Write()
        {
            Console.WriteLine("Czesci: ");
            foreach(var a in parts)
                Console.WriteLine(a);
        }
    }

    abstract class Builder
    {
        public abstract void BuildPartA();
        public abstract void BuildPartB();
        public abstract void BuildPartC();

    }
    class ConcreteBuilder : Builder
    {
        private Product product = new Product();

        public void Reset()
        {
            product = new Product();
        }

        public override void BuildPartA()
        {
            product.Add("PartA");
        }

        public override void BuildPartB()
        {
            product.Add("PartB");
        }
        public override void BuildPartC()
        {
            product.Add("PartC");
        }

        public Product GetProduct()
        {
            Product result = product;
            Reset();
            return result;
        }
    }
    class Director
    {
        Builder builder;
        public Builder Builder
        {
            set => builder = value;
        }

        public void BuildBasicProduct()
        {
            builder.BuildPartA();
        }

        public void BuildFullProduct()
        {
            builder.BuildPartA();
            builder.BuildPartB();
            builder.BuildPartC();
        }

    }
}
/*
 * Wracajac do przykladu budynku - jest on naszym produktem.
 * Budynek sklada sie ze scian, okien, drzwi itd. - sa to meotdy produktu.
 * Kazdy z naszych budowniczych (Builder) jest w stanie zbudowac budynki, ale kazdy na swoj sposob.
 * Jeden konkretny budowniczy buduje domy ze stali i szkla, inny z drewna, jeszcze inny z cegly.
 * Kazyd budowniczy bedzie wykonywal te same operacje (budowa sciany, okien, drzwi), ale ich efekt koncowy bedzie inny.
 * Dodatkowo wszystkie kroki mozna odseparowac w klasie Director - jest to kierownik budowy ktory moze przetrzymywac kolejnosc budowania czesto powtarzajacych sie typow budowli.
*/