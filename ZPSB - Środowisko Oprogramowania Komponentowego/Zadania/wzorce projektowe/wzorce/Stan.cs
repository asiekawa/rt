﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns
{
    abstract class State
    {
        public abstract void Handle(Contexx contex);
    }
    class ConcreteStateA : State
    {
        public override void Handle(Contexx contex)
        {
            contex.State = new ConcreteStateB();
        }
    }
    class ConcreteStateB : State
    {
        public override void Handle(Contexx contex)
        {
            contex.State = new ConcreteStateA();
        }
    }
    class Contexx
    {
        private State state;
        public Contexx(State state)
        {
            this.state = state;
        }
        public State State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
                Console.WriteLine("Stan: " + state.GetType().Name);
            }
        }
        public void request()
        {
            state.Handle(this);
        }
    }
}
