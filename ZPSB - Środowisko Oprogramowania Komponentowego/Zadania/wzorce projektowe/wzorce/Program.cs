﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            //singleton
            Singleton.Instance.text();
            Singleton inst1 = Singleton.Instance;
            Singleton inst2 = Singleton.Instance;
            inst1.text();
            Singleton.Instance.text();
            Console.WriteLine();

            //fabryczka
            FabrykaAbstrakcyjna fabryka1 = new FabrykaKonkretna1();
            FabrykaAbstrakcyjna fabryka2 = new FabrykaKonkretna2();
            Client cl1 = new Client(fabryka1);
            Client cl2 = new Client(fabryka2);
            cl1.Wyswietl();
            cl2.Wyswietl();

            Console.WriteLine();

            //metoda wytworcza
            Creator crt1 = new KonretnyCreator1();
            Creator crt2 = new KonretnyCreator2();
            Produktmw product1 = crt1.MetodaWytworcza();
            Produktmw product2 = crt2.MetodaWytworcza();
            Console.WriteLine(product1.GetType() + " " + crt1.GetType().Name);
            Console.WriteLine(product2.GetType() + " " + crt2.GetType().Name);

            Console.WriteLine();

            //budowniczy
            Director dyrektor = new Director();
            Builder bld = new ConcreteBuilder();
            dyrektor.Construct(bld);
            Product p1 = bld.getResult();
            p1.write();

            Console.WriteLine();

            //prototyp
            ConcretePrototype p = new ConcretePrototype("1");
            Prototype copy = p.Clone();
            Console.WriteLine("Oryginal:");
            p.write();
            Console.WriteLine("Kopia:");
            copy.write();
            Console.WriteLine();

            //WZORCE STRUKTURALNE

            //Adapter (obiektow)
            Target tgr = new Target();
            tgr.Request();
            tgr.classicRequest();

            tgr = new Adapter();
            tgr.Request();
            tgr.classicRequest();

            Console.WriteLine();

            //Most
            Abstraction abst = new refinedAbstraction();
            abst.Implementor = new ConcreteImplementorA();
            abst.Operation();
            abst.Implementor = new ConcreteImplementorB();
            abst.Operation();
/*          Bez property cos w tym stylu(pamietajcie o obiektowosci ;) ):  */
/*          abst.setImplementor(new ConcreteImplementorA());               */
/*          abst.Operation();                                              */
            Console.WriteLine();

            //Kompozyt
            Composite com1 = new Composite("korzen");
            com1.Add(new Leaf("A"));
            Leaf leaf = new Leaf("B");
            com1.Add(leaf);
            Composite com2 = new Composite("galaz");
            com2.Add(new Leaf("A2"));
            com2.Add(new Leaf("B2"));
            com1.Add(com2);
            com1.Add(new Leaf("C"));
            com1.GetChild(1);
            Console.WriteLine();
            Console.WriteLine();
            com1.Remove(leaf);
            com1.GetChild(1);
            Console.WriteLine();
            
            //Dekorator
            ConcreteDekComponent dekComp = new ConcreteDekComponent();
            ConcreteDecoratorA dekCompA = new ConcreteDecoratorA();
            ConcreteDecoratorB dekCompB = new ConcreteDecoratorB();
            dekCompA.setComponent(dekComp);
            dekCompB.setComponent(dekCompA);
            dekCompB.Operation();
            Console.WriteLine();

           
            //proxy
            Proxy proxy = new Proxy();
            proxy.request();
            Console.WriteLine();

            //metoda szablonowa
            AbstractClass absrClass = new ConcreteClass();
            absrClass.TemplateMethod();
            Console.WriteLine();

            //strategia
            Contex cont;
            cont = new Contex(new ConcreteStrategyA());
            cont.ContexInteface();
            cont = new Contex(new ConcreteStrategyB());
            cont.ContexInteface();
            cont = new Contex(new ConcreteStrategyC());
            cont.ContexInteface();
            Console.WriteLine();

            //Obserwator
            ConcreteSubject csub = new ConcreteSubject();
            csub.Attach(new ConcreteObserver(csub, "A"));
            csub.Attach(new ConcreteObserver(csub, "B"));
            csub.Attach(new ConcreteObserver(csub, "C"));
            csub.SubjectState = "123";
            csub.Notify();
            Console.WriteLine();

            //odwiedzajacy
            ObjectStructure objStr = new ObjectStructure();
            objStr.Attach(new ConcreteElementA());
            objStr.Attach(new ConcreteElementB());
            ConcreteVisitor1 v1 = new ConcreteVisitor1();
            ConcreteVisitor2 v2 = new ConcreteVisitor2();
            objStr.Accept(v1);
            objStr.Accept(v2);
            Console.WriteLine();

            //stan
            Contexx contex = new Contexx(new ConcreteStateA());

            contex.request();
            contex.request();
            contex.request();
            contex.request();
            contex.request();

            Console.WriteLine();
        }
    }
}
