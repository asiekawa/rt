#pragma once
#include "stdafx.h"

class BufferImage
{
	
private:

	glm::vec4 *buffer;
	int w;
	int h;
	
public:

	BufferImage(int width, int height);

	int getWidth();
	int getHeight();

	void Save(std::string path);
	glm::vec4& operator[](glm::ivec2 index);

	~BufferImage();
};

