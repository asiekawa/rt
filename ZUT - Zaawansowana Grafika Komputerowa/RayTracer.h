#pragma once
#include "stdafx.h"
#include "BufferImage.h"
#include "Scene.h"
class RayTracer
{
	RayTracer();
	~RayTracer();

	glm::vec3 rayTrace(Ray r, Scene Scene);

public:
	static RayTracer& getInstance();

	//dla kazdego piksela wywolujemy GetRayToPixel i zapisujemy kierunek otrzymanego promienia do pliku
	BufferImage Render(Scene Scene);

	RayTracer(RayTracer const&) = delete;
	void operator=(RayTracer const&) = delete;
};

