#pragma once
#include "stdafx.h"
using namespace std;

class Shape;

struct Ray
{
	glm::vec3 o;
	glm::vec3 d;
};

struct Segment
{
	glm::vec3 p1;
	glm::vec3 p2;
};

struct ShadingGeometry
{
	glm::vec3 hitPoint;
	glm::vec3 hitNormal;
	glm::vec2 uv;
};

struct Intersection
{
	glm::vec4 uvwt;
	std::shared_ptr<Shape> shape;
	int PrimId;
};

class Shape
{
public:
	//w klasie dziedziczacej po shape ta metoda zwraca odleglosc przeciecia zapisana w Intersection.uvwt.t
	//jesli przeciecie nie wystepuje Intersection.uvwt.t ustawiamy na -1
	virtual Intersection hitTest(Ray ray) = 0;
	//generuje geometrie cieniujaca
	virtual ShadingGeometry getShadingGeometry(Ray ray, Intersection isect) = 0;
};

class Light
{
public:
	virtual float dE(ShadingGeometry hitPoint) = 0;
	virtual Segment visibilityTester(glm::vec3 hitPoint) = 0;
};

typedef shared_ptr<Shape> ShapePtr;
typedef shared_ptr<Light> LightPtr;