#pragma once
#include "stdafx.h"
#include "Utils.h"
class Camera
{
public:

	glm::mat4x4 viewProjection;
	glm::vec3 position;
	glm::vec3 lookAt;
	glm::vec3 up;
	glm::vec2 resolution;
	float fov;
	
	Camera();
	//wypelnia powyzsze pola i tworzy macierz viewProjection
	Camera(glm::vec2 Resolution, glm::vec3 Position, glm::vec3 LookAt, glm::vec3 Up, float FOV);
	//przyjmuje wspolrzedne piksela w przedziale -1;1 i zwraca stworzony promien za pomoca macierzy viewProjection
	Ray GetRayToPixel(glm::vec2 pixel);
	~Camera();
};

