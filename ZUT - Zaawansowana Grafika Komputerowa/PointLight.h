#pragma once
#include "Utils.h"
class PointLight :
	public Light
{
public:
	glm::vec3 position;
	float intensity;

	PointLight();
	PointLight(glm::vec3 position, float intensity);
	~PointLight();
	virtual float dE(ShadingGeometry sg) override;
	virtual Segment visibilityTester(glm::vec3 hitPoint) override;
};

