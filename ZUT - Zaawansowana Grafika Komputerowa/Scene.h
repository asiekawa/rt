#pragma once
#include "stdafx.h"
#include "Camera.h"
#include "Utils.h"
using namespace std;
class Scene
{
private:
public:
	glm::vec3 ambientColor;
	glm::vec3 clearColor;
	Camera camera;
	vector<ShapePtr> shapes;
	vector<LightPtr> lights;
	Scene();
	~Scene();  

	//szuka najblizszego przeciecia pormienia z obiektami znajdujacymi sie w wektorze shapes
	Intersection Intersect(Ray ray);
	//sprawdza widocznosc pomiedzy dwoma punktami opisanymi za pomoca odcinka
	glm::vec3 IntersectP(Segment ray);
};

