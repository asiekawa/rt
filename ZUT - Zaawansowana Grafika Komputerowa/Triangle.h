#pragma once
#include "stdafx.h"
#include "Utils.h"
class Triangle :
	public Shape
{
public:
	glm::vec3 v1;
	glm::vec3 v2;
	glm::vec3 v3;
	Triangle();
	Triangle(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3);
	~Triangle();

	// Inherited via Shape
	virtual Intersection hitTest(Ray ray) override;
};

