#pragma once
#include "stdafx.h"
#include "Utils.h"
class Sphere : public Shape
{
	glm::vec3 center;
	float radius;
public:
	Sphere(glm::vec3 Center, float Radius);
	~Sphere();

	// Inherited via Shape
	virtual Intersection hitTest(Ray ray) override;
};

