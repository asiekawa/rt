clc
clear all

img = rgb2gray(imread('lena.png'));
subplot(1,3,1);
imshow(img);

F = (fft2(double(img))); %dwuwymiarowa transformata fouriera
F = fftshift(F); %przesuniecie zerowej harmonicznej na srodek widma
subplot(1,3,2);
imshow(F/20000);

%maska

F = ifftshift(F); %przesuniecie odwrotne
result = ifft2(F); %odwrotna dwuwymiarowa transformata fouriera
result = uint8(result);

subplot(1,3,3);
imshow(result);