﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Lab
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        int ImageWidht;
        int ImageHeight;
        Color[] ImageColors;

        private void OpenImage_Click(object sender, RoutedEventArgs e)
        {
            ImageColors = LoadFileColors(MyImage, out ImageWidht, out ImageHeight);
        }

        private Color[] LoadFileColors(Image dest, out int w, out int h)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            // Ustawia filtrowanie rozszerzeń pliku
            dlg.DefaultExt = ".png";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg|PNG Files (*.png)|*.png|JPEG Files (*.jpeg)|*.jpeg|GIF Files (*.gif)|*.gif";
            // Wyświtla okno dialogowe wyboru pliku 
            Nullable<bool> result = dlg.ShowDialog();
            // Jeśli wybraliśmy poprawny plik
            if (result == true)
            {
                // Wyciągnij nazwę pliku
                string filename = dlg.FileName;

                //Wczytaj obraz z pliku
                BitmapSource srcImage = new BitmapImage(new Uri(filename));

                //jeśli obraz ma inny format niż Bgra32, przekonwertuj go na Bgra32
                if (srcImage.Format != PixelFormats.Bgra32)
                    srcImage = new FormatConvertedBitmap(srcImage, PixelFormats.Bgra32, null, 0);

                // Stworz zapisywalną bitmape
                var wbitmap = new WriteableBitmap(srcImage);

                //wyciągnij informacje o obrazie
                int width = wbitmap.PixelWidth;
                int height = wbitmap.PixelHeight;
                int stride = wbitmap.BackBufferStride;
                int bytesPerPixel = (wbitmap.Format.BitsPerPixel + 7) / 8;
                w = width;
                h = height;
                Color[] arr = new Color[w * h];
                wbitmap.Lock();

                //wczytaj pixele z obrazu
                IntPtr pImgData = wbitmap.BackBuffer;
                byte[] imgData = new byte[width * height * 4];
                Marshal.Copy(pImgData, imgData, 0, width * height * 4);
                int k = 0;
                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        arr[x * height + y].B = imgData[k++];
                        arr[x * height + y].G = imgData[k++];
                        arr[x * height + y].R = imgData[k++];
                        arr[x * height + y].A = imgData[k++];
                    }
                }
                wbitmap.Unlock();

                //ustaw wyświetlanie obrazu w oknie
                dest.Source = wbitmap;
                //zwroc tablice wartosci
                return arr;
            }
            w = 0;
            h = 0;
            return null;

        }

        private void SetImageColors(Image dest, Color[] colors, int w, int h)
        {
            int k = 0;
            byte[] buffer = new byte[w*h*4];
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    buffer[k++] = colors[x * h + y].B;
                    buffer[k++] = colors[x * h + y].G;
                    buffer[k++] = colors[x * h + y].R;
                    buffer[k++] = 255;
                }
            }

            var width = w; // for example
            var height = h; // for example
            var dpiX = 96d;
            var dpiY = 96d;
            var pixelFormat = PixelFormats.Bgra32; // grayscale bitmap
            var bytesPerPixel = (pixelFormat.BitsPerPixel + 7) / 8; // == 1 in this example
            var stride = (w * pixelFormat.BitsPerPixel + 7) / 8; // == width in this example

            var bitmap = BitmapSource.Create(width, height, dpiX, dpiY,
                                             pixelFormat, null, buffer, stride);
            dest.Source = bitmap;
        }

    }
}
